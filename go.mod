module gitlab.com/72nd/acc

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/Rhymond/go-money v1.0.1
	github.com/creasty/defaults v1.3.0
	github.com/google/uuid v1.1.1
	github.com/lithammer/fuzzysearch v1.1.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/olekukonko/tablewriter v0.0.4
	github.com/phpdave11/gofpdi v1.0.8
	github.com/signintech/gopdf v0.9.5
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli/v2 v2.1.1
	gitlab.com/72th/acc v0.0.0-20200715211109-e0eed54bbb30
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
